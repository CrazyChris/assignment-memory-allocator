#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static bool heap_initialized = false;

void debug_block(struct block_header* b, const char* fmt, ...);
void debug(const char* fmt, ...);

static block_capacity capacity(size_t query) {
  return (block_capacity) { .bytes = query };
}

static block_size size(size_t query) {
  return (block_size) { .bytes = query };
}

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block) {
  return block->capacity.bytes >= query;
}
static size_t pages_count(const size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(const size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
  *((struct block_header*)addr) = (struct block_header){
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size(const size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region* r);



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap((void*)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const* addr, const block_size query) {
  const size_t reg_actual_size = region_actual_size(query.bytes);

  void* result_ptr = map_pages(addr, reg_actual_size, MAP_FIXED_NOREPLACE);
  if (result_ptr == MAP_FAILED)
    result_ptr = map_pages(addr, reg_actual_size, 0);
  if (result_ptr == MAP_FAILED)
    return REGION_INVALID;

  block_init(result_ptr, size(reg_actual_size), NULL);

  return (struct region) {
    .addr = result_ptr,
      .extends = false,
      .size = reg_actual_size
  };
}

static void* block_after(struct block_header const* block);

void* heap_init(const size_t initial) {
  const struct region region = alloc_region(HEAP_START, size(initial));
  if (region_is_invalid(&region)) return NULL;
  heap_initialized = true;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static const size_t min_block_size = offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY;

static bool block_splittable(struct block_header* restrict block, const size_t query) {
  return block->is_free && query + min_block_size <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, const size_t query) {
  if (block_splittable(block, query)) {
    const size_t left_block_capacity = size_max(query, BLOCK_MIN_CAPACITY);
    void* right_block_addr = block->contents + left_block_capacity;
    block_init(right_block_addr, size(block->capacity.bytes - left_block_capacity), block->next);
    block->next = right_block_addr;
    block->capacity = capacity(left_block_capacity);
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

// return address of something going right after the given one
static void* block_after(struct block_header const* block) {
  return  (void*)(block->contents + block->capacity.bytes);
}

// define whether second block lies right after the first one in memory
static bool blocks_continuous(
  struct block_header const* fst,
  struct block_header const* snd) {
  return (void*)snd == block_after(fst);
}

// define whether two blocks can be merged together
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* const block) {
  if (mergeable(block, block->next)) {
    const struct block_header* next_block = block->next;
    block->next = next_block->next;
    block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
    return true;
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header* block;
};

// find block that fits sz bytes size
static struct block_search_result find_good_or_last(struct block_header* restrict block, const size_t sz) {
  while (block != NULL) {
    while (block->next != NULL) {
      if (!try_merge_with_next(block))
        break;
    }
    if (block->is_free && block_is_big_enough(sz, block))
      return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = block };
    if (block->next == NULL)
      return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = block };

    block = block->next;
  }
  return (struct block_search_result) { .type = BSR_CORRUPTED, .block = NULL };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(const size_t query, struct block_header* block) {
  const struct block_search_result search_result = find_good_or_last(block, query);
  if (search_result.type != BSR_FOUND_GOOD_BLOCK) return search_result;
  struct block_header* suitable_block = search_result.block;
  split_if_too_big(suitable_block, query);
  suitable_block->is_free = false;

  return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = suitable_block };
}



static struct block_header* grow_heap(struct block_header* restrict last, const size_t query) {
  struct region region = alloc_region(block_after(last), size_from_capacity(capacity(query)));
  if (region_is_invalid(&region)) return NULL;

  region.extends = region.addr == block_after(last);
  last->next = region.addr;

  if (!try_merge_with_next(last)) return last->next;
  else return last;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(const size_t query, struct block_header* heap_start) {
  struct block_search_result search_result = try_memalloc_existing(query, heap_start);
  if (search_result.type == BSR_FOUND_GOOD_BLOCK)
    return search_result.block;
  else if (search_result.type == BSR_CORRUPTED)
    return NULL;
  else {
    struct block_header* new_block = grow_heap(search_result.block, query);
    if (new_block == NULL) return NULL;
    new_block = try_memalloc_existing(query, new_block).block;
    return new_block;
  }
}

void* _malloc(const size_t query) {
  if (!heap_initialized) return false;
  struct block_header* const addr = memalloc(query, (struct block_header*)HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*)(((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void _free(void* const mem) {
  if (!mem) return;
  struct block_header* header = block_get_header(mem);
  header->is_free = true;

  while (header->next != NULL && header->next->is_free) {
    if (!try_merge_with_next(header))
      break;
  }
}
